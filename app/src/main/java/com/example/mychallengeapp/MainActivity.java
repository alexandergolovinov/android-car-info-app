package com.example.mychallengeapp;

import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends FragmentActivity implements CustomerAdapter.ItemClicked {

    private TextView mCustomerName;
    private TextView mCustomerPhone;
    private TextView mCarInfo;
    private TextView textHeader;
    private ImageView mCarBrandImage;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter customerAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnCarInfo = findViewById(R.id.btn_car_info);
        Button btnCustomerInfo = findViewById(R.id.btn_car_owner);
        mCustomerName = findViewById(R.id.text_customer_name);
        mCustomerPhone = findViewById(R.id.text_customer_phone);
        textHeader = findViewById(R.id.text_owner_info);
        mCarBrandImage = findViewById(R.id.image_car);
        mCarInfo = findViewById(R.id.car_info);

        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);

        customerAdapter = new CustomerAdapter(this, ApplicationClass.customers);
        mRecyclerView.setAdapter(customerAdapter);

        layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);


        textHeader.setVisibility(View.GONE);
        mCustomerName.setVisibility(View.GONE);
        mCustomerPhone.setVisibility(View.GONE);

        onItemClicked(0);

        btnCarInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textHeader.setVisibility(View.GONE);
                mCustomerName.setVisibility(View.GONE);
                mCustomerPhone.setVisibility(View.GONE);
                mCarBrandImage.setVisibility(View.VISIBLE);
                mCarInfo.setVisibility(View.VISIBLE);
            }
        });

        btnCustomerInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCarBrandImage.setVisibility(View.GONE);
                mCarInfo.setVisibility(View.GONE);
                textHeader.setVisibility(View.VISIBLE);
                mCustomerName.setVisibility(View.VISIBLE);
                mCustomerPhone.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onItemClicked(int index) {
        mCustomerName.setText(ApplicationClass.customers.get(index).getName());
        mCustomerPhone.setText(ApplicationClass.customers.get(index).getPhone());
        mCarInfo.setText(ApplicationClass.customers.get(index).getCarBrand().toString());
        CarBrand carBrand = ApplicationClass.customers.get(index).getCarBrand();
        if (carBrand.equals(CarBrand.BMW)) {
            mCarBrandImage.setImageResource(R.drawable.car_black);
        } else if (carBrand.equals(CarBrand.AUDI)) {
            mCarBrandImage.setImageResource(R.drawable.car_blue);
        } else if (carBrand.equals(CarBrand.MERCEDES)) {
            mCarBrandImage.setImageResource(R.drawable.car_green);
        } else {
            mCarBrandImage.setImageResource(R.drawable.car_orange);
        }
    }
}
