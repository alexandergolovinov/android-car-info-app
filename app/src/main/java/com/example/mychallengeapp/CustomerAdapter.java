package com.example.mychallengeapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class CustomerAdapter extends RecyclerView.Adapter<CustomerAdapter.CustomerViewHolder> {

    private List<Customer> customers;
    private ItemClicked activity;

    public interface ItemClicked {
        void onItemClicked(int index);
    }

    public CustomerAdapter(Context context, List<Customer> customers) {
        this.customers = customers;
        activity = (ItemClicked) context;
    }

    public class CustomerViewHolder extends RecyclerView.ViewHolder {
        TextView mCarBrandName;
        TextView mCustomerName;
        ImageView mCarImage;

        public CustomerViewHolder(View itemView) {
            super(itemView);
            mCarBrandName = itemView.findViewById(R.id.text_car_brand);
            mCustomerName = itemView.findViewById(R.id.text_customer_name_cart);
            mCarImage = itemView.findViewById(R.id.image_car_view);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.onItemClicked(customers.indexOf((Customer) v.getTag()));
                }
            });
        }
    }

    @NonNull
    @Override
    public CustomerAdapter.CustomerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.car_single_item, parent, false);
        return new CustomerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomerAdapter.CustomerViewHolder viewHolder, int position) {
        viewHolder.itemView.setTag(customers.get(position));
        viewHolder.mCustomerName.setText(customers.get(position).getName());
        viewHolder.mCarBrandName.setText(customers.get(position).getCarBrand().toString());

        CarBrand carBrand = customers.get(position).getCarBrand();
        if (carBrand.equals(CarBrand.BMW)) {
            viewHolder.mCarImage.setImageResource(R.drawable.car_black);
        } else if (carBrand.equals(CarBrand.AUDI)) {
            viewHolder.mCarImage.setImageResource(R.drawable.car_blue);
        } else if (carBrand.equals(CarBrand.MERCEDES)) {
            viewHolder.mCarImage.setImageResource(R.drawable.car_green);
        } else {
            viewHolder.mCarImage.setImageResource(R.drawable.car_orange);
        }
    }

    @Override
    public int getItemCount() {
        return customers.size();
    }
}
