package com.example.mychallengeapp;

import android.app.Application;

import java.util.ArrayList;
import java.util.List;

public class ApplicationClass extends Application {

    public static List<Customer> customers;

    @Override
    public void onCreate() {
        super.onCreate();
        customers = new ArrayList<>();
        customers.add(new Customer("Alexander", "574234123", CarBrand.MERCEDES));
        customers.add(new Customer("Mikko", "574234123", CarBrand.BMW));
        customers.add(new Customer("Marina", "274234123", CarBrand.VOLKSVAGEN));
        customers.add(new Customer("Ville", "494234123", CarBrand.MERCEDES));
        customers.add(new Customer("Oleg", "3374234123", CarBrand.AUDI));
        customers.add(new Customer("Ksenia", "21574234123", CarBrand.NISSAN));
        customers.add(new Customer("Alexander", "574234123", CarBrand.MERCEDES));
        customers.add(new Customer("Mikko", "574234123", CarBrand.BMW));
        customers.add(new Customer("Marina", "274234123", CarBrand.VOLKSVAGEN));
        customers.add(new Customer("Ville", "494234123", CarBrand.MERCEDES));
        customers.add(new Customer("Oleg", "3374234123", CarBrand.AUDI));
        customers.add(new Customer("Ksenia", "21574234123", CarBrand.NISSAN));
        customers.add(new Customer("Alexander", "574234123", CarBrand.MERCEDES));
        customers.add(new Customer("Mikko", "574234123", CarBrand.BMW));
        customers.add(new Customer("Marina", "274234123", CarBrand.VOLKSVAGEN));
        customers.add(new Customer("Ville", "494234123", CarBrand.MERCEDES));
        customers.add(new Customer("Oleg", "3374234123", CarBrand.AUDI));
        customers.add(new Customer("Ksenia", "21574234123", CarBrand.NISSAN));
    }
}
