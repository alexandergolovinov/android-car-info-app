package com.example.mychallengeapp;

import android.print.PrinterId;

public class Customer {

    private String name;
    private String phone;
    private CarBrand carBrand;

    public Customer(String name, String phone, CarBrand carBrand) {
        this.name = name;
        this.phone = phone;
        this.carBrand = carBrand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public CarBrand getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(CarBrand carBrand) {
        this.carBrand = carBrand;
    }
}

enum CarBrand  {
    VOLKSVAGEN, NISSAN, MERCEDES, BMW, AUDI
}
